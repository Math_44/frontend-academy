import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavComponent } from './components/view/nav/nav.component';
import { SlidesComponent } from './components/view/slides/slides.component';
import { HeaderComponent } from './components/view/header/header.component';
import { MatriculaComponent } from './components/cadastro-alunos/matricula/matricula.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/view/footer/footer.component';
import { CardTrainingComponent } from './components/cards/card-training/card-training.component';
import { PagGrupoMuscularComponent } from './components/treino/dicas/pag-grupo-muscular/pag-grupo-muscular.component';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask'
 
const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SlidesComponent,
    HeaderComponent,
    MatriculaComponent,
    HomeComponent,
    FooterComponent, 
    CardTrainingComponent, 
    PagGrupoMuscularComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule, 
    HttpClientModule,
    FormsModule,
    NgxMaskModule.forRoot(maskConfig),
    ReactiveFormsModule
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
