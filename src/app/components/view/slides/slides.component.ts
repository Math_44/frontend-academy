import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.css'],
  
})
export class SlidesComponent implements OnInit {

  images = ['/assets/img/banner1.png', '/assets/img/banner2.png', '/assets/img/banner3.png'];

  constructor() {}

  ngOnInit(): void {
  }

}
