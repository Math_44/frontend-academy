export class Treinos {

    id?: string;
    name: string;
    exercicio_1: string;
    desc_1: string;
    dica_exe_1: string;
    dica_1: string;

    exercicio_2: string;
    desc_2: string;
    dica_exe_2: string;
    dica_2: string;

    exercicio_3: string;
    desc_3: string;
    dica_exe_3: string;
    dica_3: string;

    exercicio_4: string;
    desc_4: string;
    dica_exe_4: string;
    dica_4: string;

    link_1: string;
    link_2: string;


    constructor(id: string, name: string, exercicio_1: string, desc_1: string, dica_exe_1: string, dica_1: string, exercicio_2: string,
                desc_2: string, dica_exe_2: string, dica_2: string, exercicio_3: string, desc_3: string, dica_exe_3: string, dica_3: string,
                exercicio_4: string, desc_4: string, dica_exe_4: string, dica_4: string, link_1: string, link_2: string) {

        this.id = id;
        this.name = name;
        this.exercicio_1 = exercicio_1;
        this.desc_1 = desc_1;
        this.dica_exe_1 = dica_exe_1;
        this.dica_1 = dica_1;

        this.exercicio_2 = exercicio_2;
        this.desc_2 = desc_2;
        this.dica_exe_2 = dica_exe_2;
        this.dica_2 = dica_2;

        this.exercicio_3 = exercicio_3;
        this.desc_3 = desc_3;
        this.dica_exe_3 = dica_exe_3;
        this.dica_3 = dica_3;

        this.exercicio_4 = exercicio_4;
        this.desc_4 = desc_4;
        this.dica_exe_4 = dica_exe_4;
        this.dica_4 = dica_4;

    }

}
