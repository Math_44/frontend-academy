import { TestBed } from '@angular/core/testing';

import { AcademiaServiceService } from './academia-service.service';

describe('AcademiaServiceService', () => {
  let service: AcademiaServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AcademiaServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
