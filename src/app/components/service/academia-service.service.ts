import { Usuarios } from './usuarios';
import { Treinos } from './treinos';
import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AcademiaServiceService {

  constructor(private http: HttpClient) { }

  private url_1 = "http://localhost:3001/treinos";
  private url_2 = "http://localhost:3000/users";
  
  

 public getInformation(): Observable<Treinos[]>{

    return this.http.get<Treinos[]>(this.url_1);

    
  };

 public postUser(usuarios: Usuarios): Observable<Usuarios>{

    return this.http.post<Usuarios>(this.url_2, usuarios)
    .pipe(

      delay(2000)
      
    );
  
  }
  
  



  public get_id(id: string): Observable<Treinos>{

    const url = `${this.url_1}/${id}`;
    return this.http.get<Treinos>(url);


  }




}
