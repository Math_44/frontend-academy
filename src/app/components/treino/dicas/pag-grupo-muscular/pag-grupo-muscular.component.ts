import { AcademiaServiceService } from './../../../service/academia-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Treinos } from './../../../service/treinos';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pag-grupo-muscular',
  templateUrl: './pag-grupo-muscular.component.html',
  styleUrls: ['./pag-grupo-muscular.component.css']
})
export class PagGrupoMuscularComponent implements OnInit {

  constructor(private router: Router, private route : ActivatedRoute,private service: AcademiaServiceService) { }

public treino: Treinos[];

  ngOnInit(): void {

    const id= this.route.snapshot.paramMap.get('id');

    this.service.get_id(id).subscribe((response: any) => {


      this.treino = response.treino

      console.log(response);

    });


  }

}
