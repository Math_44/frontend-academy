import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagGrupoMuscularComponent } from './pag-grupo-muscular.component';

describe('PagGrupoMuscularComponent', () => {
  let component: PagGrupoMuscularComponent;
  let fixture: ComponentFixture<PagGrupoMuscularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagGrupoMuscularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagGrupoMuscularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
