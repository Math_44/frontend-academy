import { Treinos } from './../../../service/treinos';
import { AcademiaServiceService } from './../../../service/academia-service.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pag-dicas',
  templateUrl: './pag-dicas.component.html',
  styleUrls: ['./pag-dicas.component.css']
})
export class PagDicasComponent implements OnInit {

  constructor(private router: Router, private route : ActivatedRoute,private service: AcademiaServiceService) { }

  public treino: Treinos;


  ngOnInit( ): void {

    const id= this.route.snapshot.paramMap.get('id');

    this.service.get_id(id).subscribe(treino => {


      this.treino = treino;

    console.log(treino)

    });



  }


}
