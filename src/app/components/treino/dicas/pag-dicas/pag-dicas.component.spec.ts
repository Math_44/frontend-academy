import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagDicasComponent } from './pag-dicas.component';

describe('PagDicasComponent', () => {
  let component: PagDicasComponent;
  let fixture: ComponentFixture<PagDicasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagDicasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagDicasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
