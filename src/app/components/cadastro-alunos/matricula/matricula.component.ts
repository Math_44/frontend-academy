import { Usuarios } from './../../service/usuarios';
import { Router } from '@angular/router';
import { AcademiaServiceService } from './../../service/academia-service.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';



@Component({
  selector: 'app-matricula',
  templateUrl: './matricula.component.html',
  styleUrls: ['./matricula.component.css']
})
export class MatriculaComponent implements OnInit {

  usuario: Usuarios={

    nome : "",
    cpf: "",
    data_nascimento: ""

  }

  errorCadastro = false
  successCadastro = false
  errorDados = false
  errorData = false

  formulario: FormGroup;

  constructor(private formBuilder: FormBuilder, private service: AcademiaServiceService, private router: Router) {}

  ngOnInit(): void {

    this.configurarFormulario()

  }

    configurarFormulario(){

        this.formulario = this.formBuilder.group({

          nome: [null, Validators.required],
          cpf: [null, Validators.required],
          data_nascimento: [null, Validators.required],
          


        })

    }

  matriculaUsuario(): void{

    this.service.postUser(this.formulario.value).subscribe(
      
      (data) => {
      
      return this.successCadastro = true,
      this.router.navigate(['/'])
      
      
    
    },
    
    (error)=>{
    
      if(error.status== 401){

        return this.errorDados = true

      } else if(error.status == 404){

        return this.errorData = true
        
      }else {

         return this.errorCadastro = true,
      console.log(error)

      }
     

    }
    
    
    );

  }

  public close(){

    this.errorCadastro = false
    this.successCadastro = false
    this.errorDados = false
    this.errorData = false


  }

    
 
}
