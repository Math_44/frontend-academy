import { HomeComponent } from './components/home/home.component';
import { MatriculaComponent } from './components/cadastro-alunos/matricula/matricula.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagGrupoMuscularComponent } from './components/treino/dicas/pag-grupo-muscular/pag-grupo-muscular.component';
import { PagDicasComponent } from './components/treino/dicas/pag-dicas/pag-dicas.component';


const routes: Routes = [

{

path: "",
component: HomeComponent

},

{

path: "matricula",
component:MatriculaComponent

}, 

{

path: "treino",
component:PagGrupoMuscularComponent

},

{
  path: "pag/treino/:id",
  component: PagDicasComponent

},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
